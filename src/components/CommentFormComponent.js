
import React,{ Component } from 'react';
import { Card,CardImg,CardText,CardTitle,CardBody, 
            Breadcrumb,BreadcrumbItem, Button, Modal,ModalBody,ModalHeader,Row, Label, Col} from 'reactstrap';
import { Control,LocalForm,Errors } from 'react-redux-form';


const required = (val) => val && val.length;
const maxLength = (len) => (val) => !val || (val.length<=len);
const minLength = (len) => (val) => val && (val.length>=len); 

class CommentForm extends Component{

    constructor(props) {
        super(props);
        this.state ={
            isCommentOpen: false
        };
        this.toggleComment = this.toggleComment.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    toggleComment () {

        this.setState({
            isCommentOpen: !this.state.isCommentOpen
        });
    }
   handleSubmit(values){
        this.props.postComment(this.props.dishId,values.rating,values.author,values.comment);
    }
 render(){
    return(    
        <div>
            <Button outline  onClick={this.toggleComment}>
                    <span className="fa fa-pencil fa-lg mr-2" aria-hidden="true">Submit Comment</span>
            </Button>
            <Modal isOpen={this.state.isCommentOpen} toggle={this.toggleComment}>
                <ModalHeader toggle={this.toggleComment}>Submit Comment</ModalHeader>
                <ModalBody>
                    <LocalForm  onSubmit={(values) => this.handleSubmit(values)}>
                        <Row className="form-group">
                            <Col md={12}>
                                <Label htmlFor="rating">Rating</Label>
                                <Control.select model=".rating" id="rating" name="rating"
                                        className="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Col md={12}>
                                <Label htmlFor="author">Your Name</Label>
                                <Control.text   model=".author" id="author" name="author"
                                        className="form-control"
                                        validators={{
                                            required,minLength : minLength(3),maxLength: maxLength(15)
                                        }}/>
                                <Errors
                                        className="text-danger"
                                        model=".author"
                                        show= "touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}  
                                />
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Col md={12}>
                                <Label htmlFor="comment">Comment</Label>
                                <Control.textarea   model=".comment" id="comment" name="comment"
                                        className="form-control" rows="6"/>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Col md={12}>
                                <Button type="submit" name="submit" color="primary">Submit</Button>
                            </Col>
                        </Row>
                    </LocalForm>
                </ModalBody>
            </Modal>
         </div>
        )
    };
}
export default CommentForm;
