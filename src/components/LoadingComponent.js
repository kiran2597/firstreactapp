import React from 'react';

export const Loading= () => {

        return(
            <div align="center" className="col-12">   
                <span className="fa fa-spinner fa-pulse  fa-4x fa-fw text-primary"></span>
                <p>Loading. . .</p>
            </div>
        );
};