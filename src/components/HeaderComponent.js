import React, { Component } from 'react';
import { Navbar, NavbarBrand, Jumbotron, NavbarToggler, Collapse, NavItem 
        ,Nav,Modal,Label,Form,Input, ModalHeader, ModalBody, FormGroup, Button,FormFeedback} from 'reactstrap';
import { NavLink } from 'react-router-dom';
class Header extends Component{

    constructor(props){
        super(props);
        this.state={

            username:'',
            password:'',
            isNavOpen: false,
            isModalOpen: false,
            touched:{
                username: false,
                password: false
            }
        };
        this.toggleNav= this.toggleNav.bind(this);
        this.toggleModal=this.toggleModal.bind(this);
        this.handleLogin=this.handleLogin.bind(this);
        this.validate=this.validate.bind(this);
        this.handleFields=this.handleFields.bind(this);
        this.handleBlur=this.handleBlur.bind(this);
    }

    handleBlur = (field) =>(evt)=>{
        this.setState({
            touched:{
                ...this.state.touched,  [field]:true
            }
        });
    }

    handleFields({target}){
        this.setState({
            [target.name] : target.value
        });
    }

    validate(username,password){
         const errors={
            username: '',
            password: ''
        }
        if(this.state.touched.username && username.length<3 || username.length>10)
            errors.username='Username should contain minimum 3 to 10 characters';
        if( this.state.touched.password && password.length<3 || password.length>10)
            errors.password= 'Password should contains minimum 3 to 8 characters';
        
        return errors;
    }

    toggleNav(){
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }
    toggleModal(){
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleLogin(event){
        this.toggleModal();
        alert("Username: "+this.username.value+ "  Password: "+this.password.value+"  Remember: "
                    +this.remember.checked);
        event.preventDefault();
    }



    render(){

        const errors=this.validate(this.state.username,this.state.password);
        return (
            <React.Fragment>
                <Navbar dark expand="md">
                    <div className="container">
                       <NavbarToggler onClick ={this.toggleNav}/>
                        <NavbarBrand className="mr-auto" href="/">
                            <img src='assets/images/logo.png' height="30" width="41" alt="Ristorante Con Fusion"/>
                        </NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                                <NavItem>
                                     <NavLink className="nav-link" to='/home'><span
                                      className="fa fa-home fa-lg"></span>Home</NavLink>
                                </NavItem>
                                <NavItem>
                                     <NavLink className="nav-link" to='/aboutus'><span
                                      className="fa fa-info fa-lg"></span>About Us</NavLink>
                                </NavItem>
                                <NavItem>
                                     <NavLink className="nav-link" to='/menu'><span
                                      className="fa fa-list fa-lg"></span>Menu</NavLink>
                                </NavItem>
                                <NavItem>
                                     <NavLink className="nav-link" to='/contactus'><span
                                      className="fa fa-address-card fa-lg"></span>Contact Us</NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <Button outline onClick={this.toggleModal}>
                                <span className="fa fa-sign-in fa-lg">Login</span>
                            </Button>
                        </NavItem>
                    </Nav>
                </Navbar>
                <Jumbotron>
                    <div className="container">
                        <div className="row row-header">
                            <div className="col-12 col-sm-6">
                                <h1>Ristorante con Fusion</h1>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleLogin}>
                            <FormGroup>
                                <Label htmlFor="username">Username</Label>
                                <Input type="text" id="username" name="username"
                                    innerRef={(input) => this.username = input} 
                                    valid={errors.username ===''}
                                    invalid={errors.username !==''}
                                    onChange={this.handleFields}
                                    onBlur={this.handleBlur('username')}/>
                                <FormFeedback>{errors.username}</FormFeedback>
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}
                                    valid={errors.password ===''}
                                    invalid={errors.password !==''} 
                                    onChange={this.handleFields}
                                    onBlur={this.handleBlur('password')} />
                                <FormFeedback>{errors.password}</FormFeedback>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}  />
                                    Remember me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary">Login</Button>
                        </Form>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        )
    }
}

export default Header;